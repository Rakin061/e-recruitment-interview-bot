

														==============================
													     E-recruitment Interview BOT
														==============================

														
								/**
								 * @license Copyright (c) 2019, ERA-INFOTECH LIMITED. All rights reserved.
								 */											



Interview BOT is a specialized System for performing interview with the candidates applied for a particular vacancies from our e-Rrecruitment. 
Candidates can get some basic informations from this HRM-Bot as well. After successfully selected from the HR department, candiate can perform this 
interview anytime with the bot and the data provided by the users will be saved in the database. So, HR department could check the answers from 
different candidates later. 

Regarding three tier architecture the application server actually works as a loosely coupled middleware and maintains interaction with Dialogflow using Webhook 
Protocol and communcate RESTFUL Web Services with python Request-Response mechanism. All these three tier applications have to work harmonically for completing 
a successfull response to a user.

So, Interview could actually work like a wrapper over the E-recruitment and HR. Employer authorities just have to set proper questions for the candiates in 
the databases. Then BOT will automatically fetch the questions within a timeframe for distinct candidates and insert the answer given by the candidates as well.
Therefore, after carrying out interviews for any specific post a tabulated list with scoring can be generated for shortlisting candidates right away!!


# Functionalities:
===================

1. Realtime Chatting for concurrent distinct users 
2. Get response from the bot across different servers.
3. Support for Chat Transcripts ...
4. Admin Panel with statistics support added..
5. Setting Question for distinct vacancies even for specific candidates.
6. Login by entering valid Mobile Number and Name
7. Scoring after every interview added.
8. Conversation Refreshing option.
9. Admin panel for viewing unanswered questions and train for static un-chained questions responses 
10. BOT will try to keep users in a perfect track with a guided conversation tour but BOT will be capable to answer any open question within the scope.
11.	Response generation from linking with databases for some defined questions
12.	Inactive session waiting timer and alert and finally auto stop option after a defined inactive period
13.	Conversation Archiving and monitoring


# Features:
=============

1. Interactive login for every candidates.
2. Chat History/Transcript for every candidates.
3. Authentication before every interview session for selection of proper questions for the candidate.
4. Completely randomized set of questions for maintaining variations for each interview session.
5. Both MCQ Type and Written Type questions are supported.
6. Timer display for each individual questions for a session. 
7. Indenpendent Module for Question Setters of the interview.
8. Scoring Capabilities for each candidate.
9. Shortlisting candidates for a specific post possible by getting results in chronological order.
10.	Timer Feature & Auto Logout.	
11.	Intelligence of Predicting Actual User Query.
12.	Authenticated Admin Login.
13.	Statistical Data.
	




# Installation:
==================

Dev Server:

* `source virtual/bin/activate`
* `pip install -r requirements.txt --user`
* `python server.py`

Uwsgi Server:

* uwsgi --master --https :5003,foobar.crt,foobar.key --http-websockets  --gevent 1000 --wsgi-file server.py --callable app



It is highly encouraged to run the application in virtual environment. 

Regards, 
=========

Developer : Salman Rakin

Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	